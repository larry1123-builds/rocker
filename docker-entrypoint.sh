#!/bin/sh
set -e

# credit https://github.com/docker-library/docker docker-entrypoint.sh

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- rocker "$@"
fi

# if our command is a valid rocker subcommand, let's invoke it through rocker instead
# (this allows for "docker run rocker build", etc)
if rocker help "$1" > /dev/null 2>&1; then
	set -- rocker "$@"
fi

# if we have "--link some-docker:docker" and not DOCKER_HOST, let's set DOCKER_HOST automatically
if [ -z "$DOCKER_HOST" -a "$DOCKER_PORT_2375_TCP" ]; then
	export DOCKER_HOST='tcp://docker:2375'
fi

# Issue warning to use docker:dind over this image.
if [ "$1" = 'docker' -a "$2" = 'dockerd' ]; then
  if docker help > /dev/null 2>&1; then
    cat >&2 <<-'EOW'
 📎 Hey there!  It looks like you're trying to run a Docker daemon.
    You probably should use the "dind" image variant of docker instead, something like:
      docker run --privileged --name some-overlay-docker -d docker:stable-dind --storage-driver=overlay
    See https://hub.docker.com/_/docker/ for more documentation and usage examples.
EOW
    sleep 3
  fi
fi

exec "$@"
