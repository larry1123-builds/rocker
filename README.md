larry1123/rocker
=======

[![](https://images.microbadger.com/badges/version/larry1123/rocker.svg)](https://microbadger.com/images/larry1123/rocker) [![](https://images.microbadger.com/badges/image/larry1123/rocker.svg)](https://microbadger.com/images/larry1123/rocker)  
[![](https://images.microbadger.com/badges/version/larry1123/rocker:docker.svg)](https://microbadger.com/images/larry1123/rocker:docker) [![](https://images.microbadger.com/badges/image/larry1123/rocker:docker.svg)](https://microbadger.com/images/larry1123/rocker:docker)  

An alpine based build of [rocker](https://github.com/grammarly/rocker), that works like the [library/docker](https://hub.docker.com/_/docker/) image.

Usage
-------------
For usage of rocker itself view it's docs on it's [github](https://github.com/grammarly/rocker).  
The image can be used much like how the docker image can be used, [library/docker](https://github.com/docker-library/docker) for details how it can be used.  
The main reason for this image is for use in gitlab runners. If you have a build that requires rocker use this image like you would the docker image. [Gitlab Docs](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html)  

This alias is a good example of how make use of this image. (credit given bellow)  
```
$ alias rocker='docker run -v /var/run/docker.sock:/var/run/docker.sock -v "${HOME}/.docker":/root/.docker -v "${HOME}/.rocker_cache":/root/.rocker_cache -v "$(pwd)":"$(pwd)" -w $(pwd) -it --rm larry1123/rocker:1.3.1'
$ rocker -v
rocker version 1.3.1 - 5b0d185 (master) 2017-09-26_20:03_GMT
```
For details on how this works read the [How it works](https://github.com/segfly-oss/rocker#how-it-works) from segfly's version.  
The `--env-file <(printenv | grep -v HOME)` was removed because I could never run it with that added.  
Docker supports -w for what working directory to start in, so I changed to use that over the adhoc way of using the entrypoint script.  
If you have not added yourself to the docker group you may want to add `sudo` to the alias.  

Build Info
-------------
The project's home is on [gitlab](https://gitlab.com/larry1123-builds/rocker).  
Issues and merge request should be made there.  
You can view status of builds and use the gitlab image registry if you want.  
Each Image uses [dumb-init](https://github.com/Yelp/dumb-init).  
This project is hosted on [docker hub](https://hub.docker.com/r/larry1123/rocker/) also, it is not updated during the build pipeline just yet.  

Thanks
-------------
[rocker](https://github.com/grammarly/rocker) For making a build tool that you can mount volumes in, something that makes builds much cleaner for me.  
[library/docker](https://github.com/docker-library/docker) For the hard work of the docker team, and contributors that make the docker docker image. This project is setup much like it.  
[segfly/rocker](https://hub.docker.com/r/segfly/rocker/) For creating good inspiration. Any work from here may be included back under it's Apache v2 license.  

How to build
-------------
This repo is setup to be built with gitlab's ci runner. However the base image can build built locally using rocker.
